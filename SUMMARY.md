# Table of contents

* [欢迎](README.md)
* [快速开始](start.md)

## Kamiya API

* [鉴权](kamiya-api/auth.md)
* [图像生成](kamiya-api/image.md)
* [OpenAI](kamiya-api/openai.md)
* [QR Waifu](kamiya-api/qrwaifu.md)
* [计费历史](kamiya-api/billinghistory.md)
* [生成历史](kamiya-api/history.md)

## OpenID App

* [简介](openid-app/openid.md)
* [Kamiya OpenID App Doc](openid-app/openiddoc.md)
