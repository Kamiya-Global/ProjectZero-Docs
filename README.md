# 欢迎

> Kamiya致力于提供面向社区的生成式AI服务。

## 概述

你可以在这自由地里体验到由社区创造的最新的内容而无需付出任何成本。不同于其他的AI服务提供商，我们是完全**公益**且**面向社区**的。在任何时候Kamiya都不会存在强制付费，任何资源都有机会免费获得。

查看[快速开始](start.md)了解详情。

## 主页

访问 [Kamiya.Dev](https://www.kamiya.dev) 了解我们的主要业务。

## API 访问

* Stable Diffusion Image Generate
* GPT Chat Generate
* Anthropic Chat Generate(limited)
* AI Art QR Code Generate
* Global Image Delivery
* And More...

## 捐赠

如果你觉得众神之谷对您有帮助，欢迎前往 [魔法商店](https://shop.kamiya.dev) 支持我们
