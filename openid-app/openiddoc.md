# Kamiya OpenID App Doc

## 1.登录页面

将用户跳转到

```
Browser https://www.kamiya.dev/openIDAppLogin.html?appId=<Your APP ID>
```

## 2.通过Token获取用户数据

```
POST https://p0.kamiya.dev/api/session/verifyOpenIDToken
```

请求JSON示例

```json
{
    "appId": "<Your APP ID>",
    "secret": "<Your APP Secret>",
    "token": "J2C1O-4D4TQ"
}
```

返回JSON示例

```json
{
    "status": 200,
    "data": {
        "id": 1,
        "email": "example@example.com",
        "role": "Normal"
    }
}
```

其他status

404: Token不存在或存在的Token不对应请求的AppId

400: 请求格式错误

401: AppId或AppSecret错误

## 3.向用户扣费

```
POST https://p0.kamiya.dev/api/billing/charge
```

请求JSON示例

```json
{
    "appId": "<Your APP ID>",
    "secret": "<Your APP Secret>",
    "id": 1, //接口返回用户信息中的id
    "credit": 15, //魔晶数目
    "allowNC": true //允许使用非商业魔晶
}
```

返回JSON示例

```json
{
    "status": 200,
    "message": "OK"
}
```

其他status

403:

如message为 `insufficient credit` 则表示用户魔晶不足

如message为 `user not active` 则表示用户未在Kamiya验证邮箱地址或账户已禁用

400: 请求格式错误

401: AppId或AppSecret错误

## 4.授权扣费模式

### 4.1.授权用户账户

该操作旨在确认用户账户余额是否为正。

```
POST https://p0.kamiya.dev/api/billing/auth
```

请求JSON示例

```json
{
    "appId": "<Your APP ID>",
    "secret": "<Your APP Secret>",
    "id": 1 //接口返回用户信息中的id
}
```

返回JSON示例

```json
{
    "status": 200,
    "message": "OK"
}
```

其他status

403:

如message为 `insufficient credit` 则表示用户魔晶不足

如message为 `user not active` 则表示用户未在Kamiya验证邮箱地址或账户已禁用

400: 请求格式错误

401: AppId或AppSecret错误

#### 4.1.1.余量查询

```
POST https://p0.kamiya.dev/api/billing/detail
```

请求JSON示例

```json
{
    "appId": "<Your APP ID>",
    "secret": "<Your APP Secret>",
    "id": 1 //接口返回用户信息中的id
}
```

返回JSON示例

```json
{
    "status": 200,
    "message": "OK",
    "data": {
        "email": "as1583377@outlook.com",
        "role": "Administrator",
        "credit": 17328.47,
        "NCCredit": 1000, // 非商业魔晶
        "active": true
    }
}
```

#### 4.1.2.签到

```
POST https://p0.kamiya.dev/api/billing/openidCheckin
```

请求JSON示例

```json
{
    "appId": "<Your APP ID>",
    "secret": "<Your APP Secret>",
    "id": 1 //接口返回用户信息中的id
}
```

返回JSON示例

```json
{
    "status": 200,
    "message": "OK"
}
```

其他status

400:

如message为 `Bad Request` 则说明请求格式错误

如message为 `already checkin` 则说明用户今日已签到

401: AppId或AppSecret错误

### 4.2.扣费

扣费操作可在业务逻辑完成后进行，目的是方便结合业务动态扣费。该接口不检查用户余额是否大于请求扣费额。

```
POST https://p0.kamiya.dev/api/billing/forceCharge
```

请求JSON示例

```json
{
    "appId": "<Your APP ID>",
    "secret": "<Your APP Secret>",
    "id": 1, //接口返回用户信息中的id
    "credit": 15, //魔晶数目
    "allowNC": true //允许使用非商业魔晶
}
```

返回JSON示例

```json
{
    "status": 200,
    "message": "OK"
}
```

其他status

403: 如message为 `user not active` 则表示用户未在Kamiya验证邮箱地址或账户已禁用

400: 请求格式错误

401: AppId或AppSecret错误
