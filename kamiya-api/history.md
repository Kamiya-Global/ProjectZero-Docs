# 生成历史

该API现已弃用，但仍保留以维持兼容性。

## 获取历史记录

```http
GET https://p0.kamiya.dev/api/userContent/get?start=1&take=2
```

响应示例

```json
{
    "status": 200,
    "message": "OK",
    "data": [
        {
            "uuid": "7aa97681-284f-4faf-8ce3-567f3cebe141",
            "url": "https://r2.kamiya-a.tech/usercontent/7aa97681-284f-4faf-8ce3-567f3cebe141"
        },
        {
            "uuid": "a6851538-16a4-439f-8bc3-bc8618502f9f",
            "url": "https://r2.kamiya-a.tech/usercontent/a6851538-16a4-439f-8bc3-bc8618502f9f"
        }
    ]
}
```

图片倒序排列，越晚生成越靠前。

## 删除历史记录

```http
GET https://p0.kamiya.dev/api/userContent/delete?uuid=a6851538-16a4-439f-8bc3-bc8618502f9f
```

响应示例

```json
{
    "status":200,
    "message":"OK"
}
```

!>该操作不仅在数据库中删除历史记录，同时也在对象储存中永久删除该图片，操作不可逆
