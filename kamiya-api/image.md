# 图像生成

## 获取配置信息

```http
GET https://p0.kamiya.dev/api/image/config
```

响应示例过长 请见 [https://s.kmy.lol/gawol1yq](https://s.kmy.lol/gawol1yq)

## 图像生成

```http
POST https://p0.kamiya.dev/api/image/generate
```

请求示例

```json
{
    "type": "text2image", //image2image
    "prompts": "礼花，气球，红色瞳孔，小巷，看着观众，城市，浮世绘，头发飘起，",
    "negativePrompts": "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry",
    "step": 28,
    "cfg": 12,
    "seed": 218506577,
    "sampling": "DPM++ 2M Karras",
    "width": 768,
    "height": 512,
    "model": "original", //模型配置见Config
    "LoRAs": [ //LoRA配置见Config
        {
            "id": "bocchiEdStyleLora_bocchiEdStyle",
            "weight": 60
        }
    ],
    "image": "<base64 url>" //在type为image2image时传递
}
```

响应示例

```json
{
    "status": 200,
    "message": "OK",
    "data": {
        "id": "w1tjx7j",
        "hashid": "nqy1zenwuykr256",
        "metaid": "dc9d09c889017a0538ccad375339c275",
        "status": "created",
        "metadata": {
            "LoRAs": [],
            "model": "original",
            "width": 768,
            "height": 512,
            "type": "text2image",
            "prompts": "礼花，气球，红色瞳孔，小巷，看着观众，城市，浮世绘，头发飘起，",
            "negativePrompts": "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry,lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry",
            "sampling": "DPM++ 2M Karras",
            "step": 28,
            "cfg": 12,
            "seed": 218506577,
            "batch": 1,
            "artboard": "768x512"
        },
        "userId": 2,
        "createdAt": "08-06 07:30:20",
        "updatedAt": "08-06 07:30:20"
    }
}
```

## 任务追踪

根据上面API返回的`data`中的`hashid`，则可以通过以下接口获取任务状态。

```http
GET https://p0.kamiya.dev/api/image/generate/:hashid
```

响应示例

```json
{
    "status": 200,
    "message": "OK",
    "data": {
        "id": "w1tjx7j",
        "hashid": "nqy1zenwuykr256",
        "metaid": "dc9d09c889017a0538ccad375339c275",
        "status": "generated",
        "metadata": {
            "LoRAs": [],
            "model": "original",
            "width": 768,
            "height": 512,
            "type": "text2image",
            "prompts": "礼花，气球，红色瞳孔，小巷，看着观众，城市，浮世绘，头发飘起，",
            "negativePrompts": "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry,lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry",
            "sampling": "DPM++ 2M Karras",
            "step": 28,
            "cfg": 12,
            "seed": 218506577,
            "batch": 1,
            "artboard": "768x512",
            "jpg": "https://huader-output.cdn.aliceeiga.com/hrw-v3_3/outputs/text2image/2023-08-06/1691278224754-513473-dc9d09c889017a0538ccad375339c275-generated.jpg"
        },
        "userId": 2,
        "createdAt": "08-06 07:30:20",
        "updatedAt": "08-06 07:30:26"
    }
}
```

如果状态为`generated`则可以在`metadata`中读取到`jpg`属性。



## 列出任务

<pre><code><strong>GET https://p0.kamiya.dev/api/image/generate?start=1&#x26;take=1
</strong></code></pre>

响应示例

```
{
    "status": 200,
    "message": "OK",
    "data": [
        {
            "id": "lnd1zsy",
            "hashid": "58pqdly1h2pvmeg",
            "metaid": "58970eb88901230adeb3d6c7f0287024",
            "status": "generated",
            "metadata": {
                "LoRAs": [],
                "model": "original",
                "width": 768,
                "height": 512,
                "type": "text2image",
                "prompts": "礼花，气球，红色瞳孔，小巷，看着观众，城市，浮世绘，头发飘起，",
                "negativePrompts": "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry,lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry",
                "sampling": "DPM++ 2M Karras",
                "step": 28,
                "cfg": 12,
                "seed": 3050350418,
                "batch": 1,
                "artboard": "768x512",
                "jpg": ""
            },
            "userId": 2,
            "createdAt": "08-03 05:01:51",
            "updatedAt": "08-03 05:01:57"
        }
    ]
}
```
