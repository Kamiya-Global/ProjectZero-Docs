# QR Waifu

使用Kamiya提供的QR Waifu可快速生成AI艺术化的二维码，集成了短链接与二维码生成模块，无需额外的接口调用。

## 生成二维码
```http request
POST https://p0.kamiya.dev/api/image/qrwaifu
```
请求示例
```json
{
    "prompt":"1 girl",
    "type":"link", //如需缩短文段则可提交 post
    "content":"https://baidu.com",
    "steps":"28",
    "weight":"0.7", // controlnet 权重
    "guidance_start":"0.14", // controlnet 引导开始
    "guidance_end":"0.68" // controlnet 引导结束
}
```
响应示例
```json
{
    "status": 200,
    "message": "OK",
    "image": "https://r2.kamiya-a.tech/usercontent/b36c58a2-c489-4fac-8729-cae20b7b0429",
    "shortly": "https://s.kmy.lol/05nv4f02" // 二维码扫描后的内容
}
```
