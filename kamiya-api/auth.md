# 鉴权

Kamiya API使用API Key作为鉴权凭据。

### 检查API端点

```
GET https://p0.kamiya.dev/api/checkNetwork
```

返回示例

```json
{
    "status":200,
    "ip":"103.186.212.127",
    "message":"OK"
}
```

## 鉴权方式

Kamiya API使用HTTP Header中的`Authorization`字段作为鉴权方式，需要将API Key以Bearer Token的方式传入。

以获取API Key所关联的账户详情为例

```http
GET https://p0.kamiya.dev/api/session/getDetails
```

Headers应包含

```json
{
  "Authorization": "Bearer sk-cAPqidPQEZlHQ2T09iKYwOeWz9ZF5VUbbCgSPxNtivnpCV67"
}
```

返回示例

```json
{
    "status": 200,
    "message": "OK",
    "data": {
        "email": "example@example.com",
        "role": "Normal", 
        "credit": 17993.36,
        "active": true
    },
    "useNAIFallback": false
}
```
